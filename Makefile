all: algo-repl

algo-repl: main.o tree.o hash.o heap.o util.o bin
	gcc -g -lmhash -lm lib/main.o lib/tree.o lib/heap.o lib/hash.o lib/util.o -o bin/algo-repl

main.o: src/main.c lib
	gcc -g -c src/main.c -o lib/main.o

tree.o: src/tree.c lib
	gcc -g -c src/tree.c -o lib/tree.o

hash.o: src/hash.c lib
	gcc -g -c -lmhash -lm src/hash.c -o lib/hash.o

heap.o: src/heap.c lib
	gcc -g -c src/heap.c -o lib/heap.o

util.o: src/util.c lib
	gcc -g -c src/util.c -o lib/util.o

lib:
	mkdir lib

bin:
	mkdir bin

clean:
	rm -rf lib bin
