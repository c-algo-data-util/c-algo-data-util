#ifndef HASH_H
#define HASH_H

#include <string.h>
#include <math.h>
#include <stdlib.h>
#include <stdint.h>
#include <limits.h>
#include "tree.h"

#define HASH_SIZE_SMALL 257   // a sane small hash size
#define HASH_THRESHOLD  65537 // 4th fermat prime

typedef struct   hash_table hash_table;

extern int index_hash_function(char *string, int size);
extern hash_table *hash_table_init(int min_size);
extern void        destroy_hash_table(hash_table *hash);
extern int         add_to_hash(hash_table *hash, char *key, char *value);
extern char      **key_list(hash_table *hash);
extern char       *value_extract_hash(hash_table *hash, char *key);
extern void        print_hash(hash_table *hash);
extern void        hash_remove_value(hash_table *hash, char *key);

struct hash_table
{
  int size; // must be a prime number
  int count; // number of objects

  char **key;  // keys
  char **value; // hashed values as strings
};

#endif
