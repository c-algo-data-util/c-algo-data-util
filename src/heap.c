/*
   heap is a C implementations of heap sort
   Copyright (C) 2014 Sebastien Roy

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/

#include "heap.h"
#include <stdio.h>
#include <stdlib.h>

static cons *heap_from_list(cons *head);
static void print_heap(int *heap, int size);
static void max_heapify(int *heap, int size, int i);
static void heap_move_up(int *heap, int node);

extern void heap_sort(cons *head)
{
  /* an array of pointers */
  cons *sort_list = heap_from_list(head);
}

static cons *heap_from_list(cons *head)
{
  int *heap;
  int *sorted;
  cons *p;
  cons *temp;
  int nb = 0, i = 0;

  for(p = head ; p ; p = p->left_child)
    ++nb;

  heap = malloc(nb*sizeof(int));
  sorted = malloc(nb*sizeof(int));

  /* init heap */
  for(p = head ; p ; p = p->left_child){
    heap[i] = atoi(p->obj->obj);
    ++i;
  }

  print_heap(heap,nb);

  max_heapify(heap,nb,0);

  print_heap(heap,nb);

  /* sort */

  i = nb;
  while(i){
    --i;
    sorted[i] = heap[0];
    heap[0] = heap[i];
    max_heapify(heap,i,0);
  }

  print_heap(heap,nb);
  print_heap(sorted,nb);
//  free(heap);
  return head;
}

static void max_heapify(int *heap, int size, int i)
{
  //traverse the tree poping up any node that is greater than it's parent
  int parent = i;
  int left = 2*parent + 1;
  int right = 2*parent + 2;

  if(parent > size) return; // no more childs

//  printf("%i %i %i %i\n",left,heap[left],right,heap[right]);

  if(left < size && heap[left] > heap[parent])
    heap_move_up(heap,left);

  if(right < size && heap[right] > heap[parent])
    heap_move_up(heap,right);

  // explore left subtree
  max_heapify(heap,size,left);
  // explore right subtree
  max_heapify(heap,size,right);
}

static void heap_move_up(int *heap, int node)
{
  //move upward and swap along the way until
  //the heap property is established
  int parent = (node -1)/2;
  int temp;

  if(parent < 0 || node <= 0) return; // done

  if(heap[parent] < heap[node]){
    temp = heap[node];
    heap[node] = heap[parent];
    heap[parent] = temp;
    heap_move_up(heap,parent);
  }
}

static void print_heap(int *heap, int size)
{
  int i;

  for(i = 0 ; i < size ; i++)
    printf("%i ",heap[i]);
  printf("\n");
}
