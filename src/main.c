/*
   main program to test other implementations
   Copyright (C) 2014 Sebastien Roy

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/

#include <stdio.h>
#include <stdlib.h>
#include "tree.h"
#include "heap.h"
#include "hash.h"

#define INPUT_BUFFER (256)

static int parse_command(char *input);
static int balance_parenthesis(char *input);
static cons *cons_parse_list(char *input);

int main(int argc, char **argv);
int main(int argc, char **argv)
{
  char input[INPUT_BUFFER+1];
  /* read eval print loop */
  hash_table *hash = hash_table_init(257);
  char *key;
  char *value;
  int  length;

  while(1){

/*
    printf("enter a key (exit to quit)\n");
    printf("## ");
    fgets(input, INPUT_BUFFER, stdin);

    if(!strncmp(input,"exit",4)) return 0;

    length = strlen(input);
    input[length - 1] = 0; //replace end of line by end of string
    key = malloc(sizeof(char)*length);
    strncpy(key,input,length);

    printf("enter a value\n");
    printf("## ");
    fgets(input, INPUT_BUFFER, stdin);

    length = strlen(input);
    input[length - 1] = 0; //replace end of line by end of string
    value = malloc(sizeof(char)*length);
    strncpy(value,input,length);

    add_to_hash(hash,key,value);

    print_hash(hash);

    printf("query the hash\n");
    printf("## ");
    fgets(input, INPUT_BUFFER, stdin);

    input[strlen(input) - 1] = 0;

    printf("The value of %s is %s\n",input,value_extract_hash(hash,input));

    printf("remove and item from the hash\n");
    printf("## ");
    fgets(input, INPUT_BUFFER, stdin);

    input[strlen(input) - 1] = 0;

    printf("The value of %s is removed (refused to refuse)\n",input);
    if(strncmp(input,"refused",7))
      hash_remove_value(hash,input);
*/
    printf("enter a list to be sorted\n");
    printf("## ");
    fgets(input, INPUT_BUFFER, stdin);

    input[INPUT_BUFFER] = 0;

//  if(parse_command(input))
//    exit(1);
    parse_command(input);
  }

  return 0;
}

static int parse_command(char *input)
{
  int i;
  int err_par;
  cons *root;  /* tree root */
  cons *p;

  if(!strncmp(input, "exit", 4))
    exit(0);

  root = cons_parse_list(input);

  for(p = root; p ; p = p->left_child)
    printf("%s\n",p->obj->obj);

  heap_sort(root);

  return 0;
}

static cons *cons_parse_list(char *input)
{
  int i = 0, j, inc = 0;
  cons *head = NULL;
  cons *append;
  cons *p;     /* cons cell */
  cons *string = NULL;/* head of string object list */
  cons *ref;   /* left child of prev character object (next sll pointer) */
  object *obj;

  for(i = 0 ; i < INPUT_BUFFER; i++){

    if((input[i] == 32 || input[i] == 0) && inc) {

      /* recycle old list */
      obj = object_create(inc);
      j = 0;

      for(p = string; p ; p = ref){
        obj->obj[j] = p->obj->obj[0];
        ++j;
        ref = p->left_child;
      }

      delete_cons(string);
      string = NULL;

      obj->obj[j] = 0;

      p = cons_create(NULL,NULL,NULL,obj);

      if(!head) head = p;
      else APPEND_LEFT_CONS(append,p);

      append = p;
      inc = 0;

      if(input[i] == 0)
        break;

    } else {

      /* allocate a new one */
      obj = object_create(1);
      obj->obj[0] = input[i];
      p = cons_create(NULL,NULL,NULL,obj);

      if(!string) string = p;
      else APPEND_LEFT_CONS(ref,p);

      ref = p;
      ++inc;
    }
  }

  return head;
}
