#ifndef TREE_H
#define TREE_H

typedef struct cons cons;
typedef struct object object;

extern cons *cons_create(cons *, cons *, cons *, object *);
extern object *object_create(int);
extern void delete_cons(cons *cell);
extern void delete_object(object *);
extern int compare_objects(cons *cell1, cons *cell2);

#define APPEND_CONS_TYPE(c_, SIDE, obj_) \
do {\
  c_->SIDE##_child = obj_;\
} while(0)

#define APPEND_RIGHT_CONS(c_,obj_) APPEND_CONS_TYPE(c_,right,obj_)
#define APPEND_LEFT_CONS(c_,obj_)  APPEND_CONS_TYPE(c_,left,obj_)

#define APPEND_OBJ(c_,obj_) \
do{\
  c_->obj = obj_;\
}while(0)

#define PUSH_CONS_LIST(old_head_,new_head_,new_head_tail_) \
do{\
  if(new_head_){\
    APPEND_LEFT_CONS(new_head_tail_,old_head_);\
    old_head_ = new_head_;\
  }\
}while(0)

#define POP_HEAD_LIST(head_,new_) \
do{\
  if(head_){\
    cons *cons = head_; \
    head_ = cons->left_cons;\
    new_ = cons;\
  }\
}while(0)

struct object
{
  /* object implementation */
  int size;
  char *obj;
};

struct cons
{
  object *obj;
  cons *right_child;
  cons *left_child; /* next for sll/dll */
  cons *parent; /* prev for dll */
};

#endif
