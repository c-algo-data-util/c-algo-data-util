#include "util.h"

extern mem_list *ptr_mem_ref_alloc()
{
  mem_list *ptr_ref;

  MALLOC_PTR(ptr_ref, sizeof(mem_list), "memory reference");
  ptr_ref->ptr_next = NULL;

}

extern void destroy_mem_ref(mem_list *ptr_mem_ref_head)
{
  mem_list *ptr_rem, *ptr_next;

  for(ptr_rem = ptr_mem_ref_head ; ptr_rem ; ptr_rem = ptr_next){
    ptr_next = ptr_rem->ptr_next;
    free(ptr_rem->ptr);
    free(ptr_rem);
  }
}
