#ifndef UTIL_H
#define UTIL_H

#include <stdio.h>
#include <stdlib.h>

typedef struct mem_list mem_list;

struct mem_list
{
  mem_list *ptr_next;
  void     *ptr;
};

extern mem_list *ptr_mem_ref_alloc();
extern void      destroy_mem_ref(mem_list *);

#define MIN(a_,b_) (a_ < b_ ? a_ : b_)
#define MAX(a_,b_) (a_ > b_ ? a_ : b_)

#define PRN_FILE(p_, ...) \
do{\
  if ( p_ )\
    fprintf(p_, __VA_ARGS__);\
} while(0)

#define PRN_LOG(f_, ...) \
do{\
  if ( f_ )\
    fprintf(stdout, __VA_ARGS__);\
} while(0)

#define PRN_ERR(MSG) \
  do{\
    fprintf(stderr,"error: %s\n",MSG);\
    exit(1);\
  } while(0)

#define MALLOC_PTR_GC(ptr_, ptr_ref_, size_, NAME) \
do{\
  MALLOC_PTR(ptr_,size_,NAME);\
  /* mem ref */\
  ptr_->ptr_ref = ptr_mem_ref_alloc();\
  Sll_PUSH(ptr_ref_, ptr_->ptr_ref, ptr_next);\
}while(0)

#define MALLOC_PTR(ptr_, size_, NAME) \
do{\
  ptr_ = malloc(size_);\
  \
  if ( size_ && !ptr_ ) {\
    fprintf(stderr,"error: memory allocation - %s\n", NAME);\
    exit(1);\
  }\
  \
} while(0)

#define Sll_PUSH(ptr_head_, ptr_, ptr_next_) \
do{ \
  if ( !ptr_head_ ) {\
    ptr_head_ = ptr_; ptr_->ptr_next_ = NULL;\
  } else {\
    ptr_->ptr_next_ = ptr_head_; ptr_head_ = ptr_;\
  }\
} while (0)

#define Sll_COUNT(pH_, pN_, TYPE, counter_) \
do{\
  TYPE *p_;\
  counter_ = 0;\
  for(p_ = pH_ ; p_ ; p_ = p_->pN_)\
    ++counter_;\
} while(0)

#define Sll_POP(ptr_head_, ptr_rem_, ptr_prev_, ptr_next_) \
do{\
  if ( ptr_rem_ == ptr_head_ && !ptr_rem_->ptr_next_) {\
    ptr_head_ = NULL;\
  } else if ( ptr_prev_ ) {\
    ptr_prev_->ptr_next_ = ptr_rem_->ptr_next_;\
  } else {\
    ptr_head_ = ptr_rem_->ptr_next_;\
  }\
  ptr_rem_->ptr_next_ = NULL;\
} while(0)

#define Sll_FIND_POP(ptr_head_, ptr_rem_, ptr_next_, TYPE) \
do{\
  TYPE *ptr_;\
  TYPE *ptr_prev_ = NULL;\
  \
  for ( ptr_ = ptr_head_ ; ptr_ && ptr_head_ != ptr_rem_ ; ptr_ = ptr_->ptr_next_ ) {\
    ptr_prev_ = ptr_;\
    if ( ptr_prev_->ptr_next_ == ptr_rem_ ) break;\
  }\
  Sll_POP(ptr_head_, ptr_rem_,ptr_prev_,ptr_next_);\
} while(0)

#define Sll_SORT(pHead_, pNext_, CMP, TYPE, TE) \
do {\
  int count = 0;\
  int i = 0;\
  int ready;\
  TE  te_;\
  \
  TYPE **ptr_array;\
  TYPE *p_, *p1_,*p2_;\
  TYPE *pn_;\
  Sll_COUNT(pHead_,pNext_, TYPE, count);\
  MALLOC_PTR(ptr_array, sizeof(TYPE *) * count, "sort");\
  \
  /* transfer list to temp array */\
  for(p_ = pHead_ ; p_ ; p_ = pn_){\
    ptr_array[i] = p_;\
    pn_ = p_->pNext_;\
    p_->pNext_ = NULL;\
    ++i;\
  }\
  pHead_ = NULL;\
  \
  do\
  {\
    ready = 0;\
    for(i = count - 1; i > 0; --i)\
    {\
      p1_ = ptr_array[i-1];\
      p2_ = ptr_array[i];\
      CMP(te_,p1_,p2_);\
      if(te_ >= 0)\
      {\
        p_ = ptr_array[i - 1];\
        ptr_array[i - 1] = ptr_array[ i ];\
        ptr_array[ i ] = p_;\
        ready = 1;\
      }\
    }\
    for(i = 1; i < count; ++i)\
    {\
      CMP(te_,ptr_array[i - 1],ptr_array[i]);\
      if(te_ >= 0)\
      {\
        p_ = ptr_array[i - 1];\
        ptr_array[i - 1] = ptr_array[ i ];\
        ptr_array[ i ] = p_;\
        ready = 1;\
      }\
    }\
  } while(ready);\
  /* rebuild list */\
  for(i = count ; i > 0 ; i--)\
    Sll_PUSH(pHead_, ptr_array[i - 1], pNext_);\
  free(ptr_array);\
} while(0)

#endif
