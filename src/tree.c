/*
   tree is a C utility librairy for tree manipulation
   Copyright (C) 2014 Sebastien Roy

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/

/* this tree implementation reuses the lisp/scheme cons cell idea:
 * each cons is a pair of pointers that could refer to an object
 * or to another cell */

#include <stdio.h>
#include <stdlib.h>
#include "tree.h"

extern cons *cons_create(cons *parent, cons *r_child, cons *l_child, object *obj)
{
  /* create a cons cell */
  cons *cell = NULL;

  cell = malloc(sizeof(cons));

  if(cell) {
    cell->right_child = r_child;
    cell->left_child = l_child;
    cell->parent = parent;
    cell->obj = obj;
  }

  return cell;
}

extern object *object_create(int size)
{
  object *obj = malloc(sizeof(object));
  obj->size   = size + 1;
  obj->obj    = malloc((size + 1)*sizeof(char));

  obj->obj[size] = 0;

  return obj;
}

extern void delete_cons(cons *cell)
{
  if(cell->obj)
    delete_object(cell->obj);
  if(cell->right_child)
    delete_cons(cell->right_child);
  if(cell->left_child)
    delete_cons(cell->left_child);
  free(cell);
}

extern void delete_object(object *obj)
{
  free(obj->obj);
  free(obj);
}

extern int compare_objects(cons *cell1, cons *cell2)
{
  char *ch1;
  char *ch2;
  int size;
  int i;
  int diff = 0;

  if(!cell1)
    return -1;

  if(!cell2)
    return 1;

  ch1 = cell1->obj->obj;
  ch2 = cell2->obj->obj;
  size = (cell1->obj->size < cell2->obj->size) ? cell1->obj->size : cell2->obj->size;

  for(i = 0 ; i < size ; i++)
    if(cell1->obj->obj[i] != cell2->obj->obj[i])
      diff = cell1->obj->obj[i] - cell2->obj->obj[i];

  if(!diff)
    diff = cell1->obj->size - cell2->obj->size;

  return diff;
}
