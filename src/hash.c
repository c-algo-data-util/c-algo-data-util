/*
   main program to test other implementations
   Copyright (C) 2014 Sebastien Roy

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/

#include "hash.h"
#include <mhash.h>
//#include <stdio.h>

static int next_prime_number(int number);
static int hash_table_increase(hash_table *hash);
static uint32_t multi_mod(uint32_t *numbers, int size, int mod);

extern int index_hash_function(char *string, int size)
{
  int index;
  uint32_t digest[8];
  MHASH mhash_sha256;

  if((mhash_sha256 = mhash_init(MHASH_SHA256)) == MHASH_FAILED)
    exit(1);

  mhash(mhash_sha256,string,size);

  mhash_deinit(mhash_sha256,digest);

  // property: (a+b) % n = ((a % n) + (b % n)) % n
  index = multi_mod(digest,8,257);

  return index;
}

static uint32_t multi_mod(uint32_t *numbers, int size, int mod)
{
  if(size == 0)
    return numbers[0] % mod;
  else
    return (numbers[size-1] % mod + multi_mod(numbers, size - 1, mod)) % mod;
}

extern hash_table *hash_table_init(int min_size)
{
  hash_table *hash;
  int i;

  if(!(hash = malloc(sizeof(hash_table))))
    return NULL;

  hash->size = next_prime_number(min_size - 1);
  /* create an empty hash */
  if(!(hash->key = malloc(hash->size*(sizeof(char *))))){
    free(hash);
    return NULL;
  }

  if(!(hash->value = malloc(hash->size*(sizeof(char *))))){
    free(hash->key);
    free(hash);
    return NULL;
  }

  hash->count = 0;

  for(i = 0; i < hash->size ; i++){
    hash->key[i] = NULL;
    hash->value[i] = NULL;
  }

  return hash;
}

extern void destroy_hash_table(hash_table *hash)
{
  int i;

  for(i = 0 ; i < hash->size ; i++)
    if(hash->key[i]){
      free(hash->key[i]);
      free(hash->value[i]);
    }

  free(hash->key);
  free(hash->value);
  free(hash);
}

/* upon successful addition of new key-value pair, return 0 1 otherwise */
extern int add_to_hash(hash_table *hash, char *key, char *value)
{
  /* key and value should be pointer to safely preallocated strings */
  int index;

  if(hash->count + 1 > hash->size)
    if(hash_table_increase(hash))
      return 1;

  ++hash->count;

  index = index_hash_function(key,strlen(key));

  if(hash->key[index]) printf("Key %s exists, replacing with %s (%s)\n",hash->key[index],key,value);

  hash->key[index] = key;
  hash->value[index] = value;

  return 0;
}

extern char **key_list(hash_table *hash)
{
  return hash->key;
}

extern char *value_extract_hash(hash_table *hash, char *key)
{
  int index = index_hash_function(key,strlen(key));

  return hash->value[index];
}

extern void hash_remove_value(hash_table *hash, char *key)
{
  int index = index_hash_function(key,strlen(key));
  char *ref_key = hash->key[index];
  char *value = hash->value[index];

  printf("index %i %s\n",index,key);

  if(hash->value[index]){
    hash->value[index] = NULL;
    hash->key[index] = NULL;
    hash->count--;
    free(value);
    free(ref_key);
  }
}

extern void print_hash(hash_table *hash)
{
  int i;

  printf("hash with %i (%i) elements:\n",hash->count,hash->size);

  for(i = 0; i < hash->size; i++)
    if(hash->key[i]) printf("   %s : %s (%i)\n",hash->key[i],hash->value[i],i);
}

static int next_prime_number(int number)
{
  // given a number (any) find the first prime number that is larger
  // than number.
  int new_number = number + 1; //new_number is greater than number
  int i;
  int div = sqrt(new_number); // the largest factor of number is such that div*div = number_number

  if(number > HASH_THRESHOLD)
    return INT_MIN; // sentinel value -> hash is full

  for(i = div ; i > 1 ; i--)
    if(!(new_number % i)) // if i is a factor of new_number is not a prime, look further
      return next_prime_number(new_number);

  return new_number;
}

/* return 1 if size increase fails or is forbidden */
static int hash_table_increase(hash_table *hash)
{
  /* precondition: input hash must be valid i.e. size > count */
  int    i;
  int    new_size,old_size = hash->size;
  char  **tmp_key;
  char  **tmp_value;

  if(old_size + 1 > HASH_THRESHOLD
     || (new_size = next_prime_number(old_size)) > HASH_THRESHOLD)
    return 1;

  /* mv old key to tmp key */
  tmp_key = hash->key;

  /* reallocate the hash */
  if(!(hash->key = malloc(new_size*(sizeof(char *))))){
    hash->key = tmp_key;
    return 1;
  }

  tmp_value = hash->value;

  if(!(hash->value = malloc(new_size*(sizeof(char *))))){
    hash->key   = tmp_key;
    hash->value = tmp_value;
    return 1;
  }

  hash->size = new_size;

  for(i = 0 ; i < old_size ; i++)
    if(tmp_key[i])
      add_to_hash(hash,tmp_key[i],tmp_value[i]);

  /* clear old memory */
  free(tmp_key);
  free(tmp_value);

  return 0;
}
